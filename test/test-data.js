test( "load from data source", function() {
  $fixture.append( '<script id="data-source" type="application/json"> [' +
    '{"name": "Apples", "unit_price": "1.49"},' +
    '{"name": "Bananas", "unit_price": "0.79"},' +
    '{"name": "Cashews", "unit_price": "4.15"},' +
    '{"name": "Protein Powder", "unit_price": "21.02"}' +
    ' ]</scr' + 'ipt>'); //cheat the browser html parser

  var source = DOM.findById("data-source")
  var items = Data.load(source)
  ok( items.length > 0, "Loaded items" );
  lastItem = items[items.length - 1]
  equal(lastItem.name, "Protein Powder");
  equal(typeof(lastItem.unit_price), "number", "Numeric unit price");
  equal(lastItem.unit_price, 21.02);
}); 
