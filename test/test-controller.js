function mockView() {
   return {
        items: [],
        total: "",
        selectedItems: [],
        addItem: function(item) {
            this.items.push(item)
        },
        askForAmount: function() {
            return 5
        },
        registerItemSelected: function(handler){},
        registerItemUnselected: function(handler){},
        registerItemQuantityChanged: function(handler){},
        registerItemAdded: function(handler){},
        registerPaymentChanged: function(handler){},
        registerShowChange: function(handler){},
        updateAmount: function() {},
        selectItem: function(description, quantity, subtotal) {
            this.selectedItems.push([description, quantity, subtotal])
        },
        setPaymentStatus: function(status, change) {
            this.paymentStatus = status
            this.change = change
        },
        removeSelectedItem: function(index) {
            this.selectedItems.splice(index, 1)
        },
        setTotal: function(totalText) {
            this.total = totalText
        }
    }
}

test("load items selector", function() {
    var v = mockView()
    var controller = new RegisterController(v)
    controller.initialize([{name: "item 1", unit_price: 2.55}])
    equal(v.items[0].name, "item 1")
}); 

test("item selected", function() {
    var v = mockView()
    var controller = new RegisterController(v)
    controller.onItemSelected({name: "item 2", unit_price: 0.50})
    equal(v.selectedItems[0][0], "item 2")
    equal(v.selectedItems[0][1], 5)
    equal(v.selectedItems[0][2], 2.5)
})

test("don't select on broken amount input", function() {
    var v = mockView()
    v.askForAmount = function() { return NaN }

    var controller = new RegisterController(v)
    controller.onItemSelected({name: "item 2", unit_price: 1.00})
    equal(v.selectedItems.length, 0)
})

test("calculate total", function() {
    var v = mockView()
    var controller = new RegisterController(v)
    controller.onItemSelected({name: "item 2", unit_price: 0.50})
    controller.onItemSelected({name: "item 3", unit_price: 0.21})
    equal(controller.total, 3.55)
    equal(v.total, "3.55")
})

test("unselect item", function() {
    var v = mockView()
    var controller = new RegisterController(v)
    controller.onItemSelected({name: "item 2", unit_price: 0.50})
    controller.onItemSelected({name: "item 3", unit_price: 0.21})
    controller.onItemUnselected(0)

    equal(controller.selectedItems.length, 1)
    equal(v.selectedItems.length, 1)
    ok(controller.total - 1.05 < 0.0001) //yay double vals and precision loss!
    equal(v.total, "1.05")
})

test("edit quantity", function() {
    var v = mockView()
    var controller = new RegisterController(v)
    controller.onItemSelected({name: "item 2", unit_price: 0.50})
    controller.onItemSelected({name: "item 3", unit_price: 0.21})
    controller.onItemQuantityChanged(0, 1)

    equal(v.selectedItems.length, 2)
    ok(controller.total - 1.55 < 0.0001)
    equal(v.total, "1.55")
})

test("add item to selector", function() {
    var v = mockView()
    var controller = new RegisterController(v)
    controller.initialize([{name: "item 1", unit_price: 2.55}])
    controller.onItemAdded("name: 'Pears', unit_price: '0.49'") 
    equal(v.items.length, 2)
    equal(v.items[1].name, "Pears")
    equal(v.items[1].unit_price, 0.49)
}); 

test("not adding item with invalid input", function() {
    var v = mockView()
    var controller = new RegisterController(v)
    controller.initialize([{name: "item 1", unit_price: 2.55}])
    controller.onItemAdded("name: 'Pears") 
    equal(v.items.length, 1)
}); 

test("payment change status", function() {
    var v = mockView()
    var controller = new RegisterController(v)
    controller.initialize([{name: "item 1", unit_price: 2.55}])
    controller.onItemSelected({name: "item 1", unit_price: 0.15})
    controller.onPaymentChanged("3") 
    equal(v.paymentStatus, "payment-ok")
    equal(v.change, 2.25)
    controller.onPaymentChanged("0.10") 
    equal(v.paymentStatus, "payment-notenough")
    equal(v.change, 0)
    controller.onPaymentChanged("jibbajabba!") 
    equal(v.paymentStatus, "payment-error")
    equal(v.change, 0)
    controller.onPaymentChanged("5.6.7") 
    equal(v.paymentStatus, "payment-error")
    equal(v.change, 0)
});

test("change denominations", function() {
    var v = mockView()
    var controller = new RegisterController(v)
    var denominations = controller.changeDenominations(0)
    equal(displayDenominations(denominations), "Exact amount.")
    denominations = controller.changeDenominations(0.25)
    equal(denominations[0], "1 Quarter (0.25 USD)")
    denominations = controller.changeDenominations(0.50)
    equal(denominations[0], "2 Quarters (0.25 USD)")
    denominations = controller.changeDenominations(0.65)
    equal(denominations[0], "2 Quarters (0.25 USD)")
    equal(denominations[1], "1 Dime (0.10 USD)")
    equal(denominations[2], "1 Nickel (0.05 USD)")

    denominations = controller.changeDenominations(0.65)
    equal(displayDenominations(denominations), "2 Quarters,1 Dime,1 Nickel")
    denominations = controller.changeDenominations(0.66)
    equal(displayDenominations(denominations), "2 Quarters,1 Dime,1 Nickel,1 Penny")
    denominations = controller.changeDenominations(0.69)
    equal(displayDenominations(denominations), "2 Quarters,1 Dime,1 Nickel,4 Pennies")
    denominations = controller.changeDenominations(2.43)
    equal(displayDenominations(denominations), "2 dollars,1 Quarter,1 Dime,1 Nickel,3 Pennies")
    denominations = controller.changeDenominations(0)
    equal(displayDenominations(denominations), "Exact amount.")
})

function displayDenominations(denominations) {
    return denominations.join(",").replace(/ \([^)]+\)/g, "")
}
