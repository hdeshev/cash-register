Data = {

    load: function(element) {
        var jsonText = element.innerHTML
        var parsed = JSON.parse(jsonText)

        for (var i = 0; i < parsed.length; i++) {
            var item = parsed[i]
            item.unit_price = parseFloat(item.unit_price, 10)
        }
        return parsed
    },

    parseItem: function(inputString) {
        var matcher = /name\s*:\s*'([a-z]+)'\s*,\s*unit_price\s*:\s*'(\d+(\.\d+)?)'\s*/i
        var match = matcher.exec(inputString)
        if (match) {
            return {
                name: match[1],
                unit_price: parseFloat(match[2], 10)
            }
        } else {
            return null
        }
    }
}

