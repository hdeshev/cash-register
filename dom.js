DOM = {}

DOM.attachEvent = function(target, eventName, handler) {
    if (window.addEventListener) {
        target.addEventListener(eventName, handler, false);
    } else if (window.attachEvent) {
        target.attachEvent("on" + eventName, handler);
    } else {
        target["on" + eventName] = handler
    }
}

DOM.findById = function(id) {
    return document.getElementById(id)
}

DOM.findByClass = function(root, className) {
    //inefficient, but quick to implement
    var elements = root.getElementsByTagName("*")
    for (var i = 0, len = elements.length; i < len; i++) {
        var element = elements[i]
        if (element.className.indexOf(className) >= 0)
            return element
    }
    return null;
}
