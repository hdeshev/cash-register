function RegisterView() {
    this.items = []
    this.itemSelectedHandler = function(){}
    this.itemUnselectedHandler = function(){}
    this.itemQuantityChangedHandler = function(){}
    this.itemAddedHandler = function(){}
    this.paymentChangedHandler = function(){}
    this.showChangeHandler = function(){}
}

RegisterView.prototype = {
    initialize: function(container) {
        this.selector = DOM.findByClass(container, "items-selector")
        this.itemsTable = DOM.findByClass(container, "items-view")
        this.totalBox = DOM.findByClass(container, "total-box")
        this.addItemButton = DOM.findByClass(container, "add-item-button")
        this.paymentBox = DOM.findByClass(container, "paid-box")
        this.changeBox = DOM.findByClass(container, "change-box")
        this.showChangeButton = DOM.findByClass(container, "show-change-button")
    },

    addItem: function(item) {
        this.items.push(item)
        var text = item.name + " ($" + item.unit_price + ")"
        var index = this.items.length

        var options = this.selector.options;
        options[options.length] = new Option(text, index)
    },

    registerItemSelected: function(handler) {
        this.itemSelectedHandler = handler
        var view = this
        var selectChange = function(){
            var index = view.selector.selectedIndex
            var item = view.items[index]
            view.itemSelectedHandler(item)
        }

        DOM.attachEvent(this.selector, "change", selectChange)
    },

    registerItemUnselected: function(handler) {
        this.itemUnselectedHandler = handler
    },

    registerItemQuantityChanged: function(handler) {
        this.itemQuantityChangedHandler = handler
    },

    registerItemAdded: function(handler) {
        var view = this
        this.itemAddedHandler = handler
        DOM.attachEvent(this.addItemButton, "click", function() {
            var input = prompt("New item value?")
            view.itemAddedHandler(input)
        })
    },

    registerPaymentChanged: function(handler) {
        var view = this
        this.paymentChangedHandler = handler
        DOM.attachEvent(this.paymentBox, "change", function(e) {
            view.paymentChangedHandler(view.paymentBox.value)
        })
    },

    registerShowChange: function(handler) {
        var view = this
        this.showChangeHandler = handler
        DOM.attachEvent(this.showChangeButton, "click", function() {
            view.showChangeHandler()
            return false
        })
    },

    askForAmount: function() {
        var input = prompt("How many?", "1")
        return parseInt(input, 10)
    },

    selectItem: function(description, quantity, subtotal) {
        var tbody = this.itemsTable.tBodies[0]
        var row = tbody.insertRow()
        var view = this
        var itemIndex = tbody.rows.length - 1

        var descriptionCell = row.insertCell(0)
        descriptionCell.innerHTML = description

        this.renderQuantityCell(row, itemIndex, quantity, subtotal)
        this.renderSubtotalCell(row, subtotal)

        var removeCell = row.insertCell(3)
        removeCell.innerHTML = "<a href='#' title='Remove'>x</a>"
        var removeButton = removeCell.getElementsByTagName("a")[0]

        var removeHandler = function() {
            view.itemUnselectedHandler(itemIndex)
            return false;
        }
        DOM.attachEvent(removeButton, "click", removeHandler)
    },

    renderQuantityCell: function(row, itemIndex, quantity, subtotal) {
        var view = this
        var quantityCell = row.cells[1]
        if (quantityCell == null)
            quantityCell = row.insertCell(1)

        quantityCell.innerHTML = "<a href='#' title='Change'>" + quantity + "</a>"
        var changeButton = quantityCell.getElementsByTagName("a")[0]
        var changeHandler = function() {
            var input = prompt("New quantity?", quantity)
            var newQuantity = parseInt(input, 10)
            if (isNaN(newQuantity))
                return false;

            view.itemQuantityChangedHandler(itemIndex, newQuantity)
            return false;
        }
        DOM.attachEvent(changeButton, "click", changeHandler)
    },

    renderSubtotalCell: function(row, subtotal) {
        var subtotalCell = row.cells[2]
        if (subtotalCell == null)
            subtotalCell = row.insertCell(2)

        subtotalCell.innerHTML = subtotal
    },

    updateAmount: function(index, quantity, subtotal) {
        var tbody = this.itemsTable.tBodies[0]
        var row = tbody.rows[index]
        this.renderQuantityCell(row, index, quantity, subtotal)
        this.renderSubtotalCell(row, subtotal)
    },

    removeSelectedItem: function(index) {
        var tbody = this.itemsTable.tBodies[0]
        tbody.deleteRow(index)
    },

    setTotal: function(totalText) {
        this.totalBox.value = totalText
    },

    setPaymentStatus: function(status, change) {
        var classes = this.paymentBox.className
        classes = classes.replace(/payment-[a-z]+/i, status)
        this.paymentBox.className = classes
        this.changeBox.value = change
    },

    showChangeWindow: function(denominations) {
        var header = '<h3>Change <a href="#" class="close-button">(Close)</a></h3>' +
            '<div class="content"><ul class="change-list">'

        var footer = '</ul></div>'
        var body = ''
        for (var i = 0; i < denominations.length; i++) {
            body += '<li>' + denominations[i] + '</li>'
        }
        var popup = document.createElement("div")
        popup.className = "change-popup"
        popup.innerHTML = header + body + footer
        var closeButton = DOM.findByClass(popup, "close-button")
        DOM.attachEvent(closeButton, "click", function() {
            popup.parentNode.removeChild(popup)
        })
        document.body.appendChild(popup)
    }
}
