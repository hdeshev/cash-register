function RegisterController(view) {
    this.items = []
    this.view = view
    this.total = 0
    this.payment = 0
    this.change = 0
    this.selectedItems = []
}

RegisterController.prototype = {
    initialize: function(items) {
        this.items = items
        for (var i = 0 ; i < items.length; i++) {
            this.view.addItem(items[i])
        }

        var self = this;
        var selectedHandler = function(item) { self.onItemSelected(item) }
        this.view.registerItemSelected(selectedHandler)
        var unselectedHandler = function(index) { self.onItemUnselected(index) }
        this.view.registerItemUnselected(unselectedHandler)
        var quantityChangedHandler = function(index, amount) { self.onItemQuantityChanged(index, amount) }
        this.view.registerItemQuantityChanged(quantityChangedHandler)
        var itemAddedHandler = function(inputString) { self.onItemAdded(inputString) }
        this.view.registerItemAdded(itemAddedHandler)
        var paymentChangedHandler = function(payment) { self.onPaymentChanged(payment) }
        this.view.registerPaymentChanged(paymentChangedHandler)
        var showChangeHandler = function() { self.onShowChange() }
        this.view.registerShowChange(showChangeHandler)

        this.updateViewTotal()
    },

    onItemSelected: function(item) {
        var amount = this.view.askForAmount()
        if (isNaN(amount))
            return

        this.selectedItems.push([item, amount])

        var description = item.name
        var quantity = amount
        var subtotal = this.itemSubtotal(item, quantity)
        this.view.selectItem(description, quantity, subtotal)

        this.total += subtotal
        this.updateViewTotal()
    },

    updateViewTotal: function() {
        this.view.setTotal(this.roundTwoDigits(this.total))
        this.updatePayment()
    },

    onItemUnselected: function(itemIndex) {
        var selected = this.selectedItems[itemIndex]
        var subtotal = this.itemSubtotal(selected[0], selected[1])
        this.total -= subtotal
        this.selectedItems.splice(itemIndex, 1)

        this.updateViewTotal()
        this.view.removeSelectedItem(itemIndex)
    },

    onItemQuantityChanged: function(itemIndex, newQuantity) {
        var selected = this.selectedItems[itemIndex]
        var subtotal = this.itemSubtotal(selected[0], selected[1])
        this.total -= subtotal

        selected[1] = newQuantity
        subtotal = this.itemSubtotal(selected[0], selected[1])
        this.total += subtotal

        this.view.updateAmount(itemIndex, newQuantity, subtotal)
        this.updateViewTotal()
    },

    itemSubtotal: function(item, quantity) {
        return this.roundTwoDigits(quantity * item.unit_price)
    },

    roundTwoDigits: function(num) {
        return Math.round(num * 100) / 100 //round to 2-nd decimal
    },

    onItemAdded: function(inputString) {
        var item = Data.parseItem(inputString)
        if (item != null) {
            this.items.push(item)
            this.view.addItem(item)
        }
    },

    onShowChange: function() {
        this.view.showChangeWindow(this.changeDenominations(this.change))
    },

    onPaymentChanged: function(newPayment) {
        this.payment = parseFloat(newPayment, 10)
        this.updatePayment(newPayment)
    },

    updatePayment: function(input) {
        var invalid = !this.paymentValid(input || "")
        this.change = this.payment - this.total
        if (invalid || this.change < 0 || isNaN(this.change))
            this.change = 0
        this.change = this.roundTwoDigits(this.change)

        if (invalid || isNaN(this.payment)) {
            this.view.setPaymentStatus("payment-error", this.change)
        } else if (this.payment > this.total) {
            this.view.setPaymentStatus("payment-ok", this.change)
        } else {
            this.view.setPaymentStatus("payment-notenough", this.change)
        }
    },

    paymentValid: function(input) {
        var matcher = /^\d*(\.\d+)?$/i
        return matcher.test(input)
    },

    changeDenominations: function(change) {
        var dollars = Math.floor(change)
        if (dollars > 0)
            change -= dollars

        var quarters = 0; var dimes = 0; var nickels = 0; var pennies = 0;
        while (change >= 0.25) {
            quarters++
            change -= 0.25
        }
        while (change >= 0.10) {
            dimes++
            change -= 0.10
        }
        while (change >= 0.05) {
            nickels++
            change -= 0.05
        }
        while (change > 0.001) { //double precision loss
            pennies++
            change -= 0.01
        }

        var result = []
        if (dollars > 0)
            result.push(this.formatDenomination(dollars, "dollar", "dollars"))
        if (quarters > 0)
            result.push(this.formatDenomination(quarters, "Quarter", "Quarters", "0.25"))
        if (dimes > 0)
            result.push(this.formatDenomination(dimes, "Dime", "Dimes", "0.10"))
        if (nickels > 0)
            result.push(this.formatDenomination(nickels, "Nickel", "Nickels", "0.05"))
        if (pennies > 0)
            result.push(this.formatDenomination(pennies, "Penny", "Pennies", "0.01"))
        if (result.length == 0)
            result.push("Exact amount.")
        return result
    },

    formatDenomination: function(amount, singular, plural, value) {
        var leading = null
        if (amount > 1)
            leading = amount + " " + plural
        else
            leading = amount + " " + singular

        if (value)
            return leading + " (" + value + " USD)"
        else
            return leading
    }
}
